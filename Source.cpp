#include <iostream>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <process.h>
#include <fstream>
#include <list>
#include "Header.h"

using namespace std;


int main(int argc, char* argv[])
{
	if (argc != 3) return 0;
	ifstream infile(argv[1]);
	ofstream outfile(argv[2]);
	list<string> list1;
	string line;
	while (!infile.eof()) {
		getline(infile, line);
		list1.push_back(line);
	}
	infile.close();
	Module1::sort_strings(list1);
	while (list1.size())
	{
		line = list1.front();
		list1.pop_front();
		outfile << line;
		if (list1.size())
			outfile << endl;
	}
}
